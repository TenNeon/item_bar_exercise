﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoxTest : MonoBehaviour {

	// Use this for initialization
	void Start () {
		UIUtility.Fill = new Color(0.9f, 0.9f, 1f);
		UIUtility.Stroke = new Color(.8f,.4f,0);
		UIUtility.StrokeWidth = 2;
		UIUtility.Padding = new RectOffset(5,5,3,3);
		

		UIUtility.AttachCanvas(gameObject, RenderMode.ScreenSpaceOverlay);

		UIUtility.FontSize = 24;
		UIUtility.TextColor = Color.red;
		UIUtility.Box(new Rect(0, 100, 200, 24), transform, "Sample text");

		UIUtility.FontSize = 36;
		UIUtility.TextColor = Color.blue;
		UIUtility.Box(new Rect(0, 0, 200, 36), transform, "Sample text");

		UIUtility.FontSize = 48;
		UIUtility.TextColor = new Color(0.0f, 0.5f, 0.0f);
		UIUtility.Box(new Rect(0, -100, 300, 48), transform, "Sample text");

	}
}
