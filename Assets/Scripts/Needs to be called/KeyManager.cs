﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KeyManager : MonoBehaviour {

	public static Dictionary<string, Keybind> bindings;
	public static Dictionary<string, Key> keys;

	static int[] keyCodes;
	public static int keysPressed;

	public delegate bool KeyCheckDelegate();

	enum KeyEventType {KeyDown, KeyPressed, KeyUp };

	public static void Init()
	{
		keys = new Dictionary<string, Key>();
		SetupKeyTracker();
		InitKeysFromKeyCodes();
		keys.Add("ScrollUp", new ScrollKey(ScrollKey.Direction.Up));
		keys.Add("ScrollDown", new ScrollKey(ScrollKey.Direction.Down));

		AddKeybind("hotbar 1", keys["Alpha1"] );
		AddKeybind("hotbar 2", keys["Alpha2"]);
		AddKeybind("hotbar 3", keys["Alpha3"]);
		AddKeybind("hotbar 4", keys["Alpha4"]);
		AddKeybind("hotbar 5", keys["Alpha5"]);
		AddKeybind("hotbar 6", keys["Alpha6"]);
		AddKeybind("hotbar 7", keys["Alpha7"]);
		AddKeybind("hotbar 8", keys["Alpha8"]);
		AddKeybind("hotbar 9", keys["Alpha9"]);
		AddKeybind("hotbar 10",keys["Alpha0"]);
		
		AddKeybind("tab 1", new List<Key>() { keys["LeftShift"], keys["Alpha1"] });
		AddKeybind("tab 1", new List<Key>() { keys["RightShift"], keys["Alpha1"] });

		AddKeybind("tab 2", new List<Key>() { keys["LeftShift"], keys["Alpha2"] });
		AddKeybind("tab 2", new List<Key>() { keys["RightShift"], keys["Alpha2"] });

		AddKeybind("tab 3", new List<Key>() { keys["LeftShift"], keys["Alpha3"] });
		AddKeybind("tab 3", new List<Key>() { keys["RightShift"], keys["Alpha3"] });

		AddKeybind("tab 4", new List<Key>() { keys["LeftShift"], keys["Alpha4"] });
		AddKeybind("tab 4", new List<Key>() { keys["RightShift"], keys["Alpha4"] });

		AddKeybind("tab 5", new List<Key>() { keys["LeftShift"], keys["Alpha5"] });
		AddKeybind("tab 5", new List<Key>() { keys["RightShift"], keys["Alpha5"] });

		AddKeybind("tab 6", new List<Key>() { keys["LeftShift"], keys["Alpha6"] });
		AddKeybind("tab 6", new List<Key>() { keys["RightShift"], keys["Alpha6"] });

		AddKeybind("tab 7", new List<Key>() { keys["LeftShift"], keys["Alpha7"] });
		AddKeybind("tab 7", new List<Key>() { keys["RightShift"], keys["Alpha7"] });

		AddKeybind("action 1", keys["Mouse0"]);
		AddKeybind("action 1", keys["E"]);

		AddKeybind("action 2", keys["Mouse1"]);
		AddKeybind("action 2", keys["Q"]);

		AddKeybind("shift", keys["LeftShift"]);
		AddKeybind("shift", keys["RightShift"]);

		AddKeybind("next tab", keys["Tab"]);
		AddKeybind("next tab", new List<Key>() {keys["LeftShift"], keys["ScrollDown"] });
		AddKeybind("next tab", new List<Key>() {keys["RightShift"], keys["ScrollDown"] });

		AddKeybind("previous tab", new List<Key>() { keys["LeftShift"], keys["Tab"]});
		AddKeybind("previous tab", new List<Key>() { keys["RightShift"], keys["Tab"]});
		AddKeybind("previous tab", new List<Key>() { keys["LeftShift"], keys["ScrollUp"]});
		AddKeybind("previous tab", new List<Key>() { keys["RightShift"], keys["ScrollUp"]});

		AddKeybind("mystery action");
	}

	private static void SetupKeyTracker()
	{
		keyCodes = (int[])System.Enum.GetValues(typeof(KeyCode));
	}

	static void InitKeysFromKeyCodes()
	{
		for (int i = 0; i < keyCodes.Length; i++)
		{
			string keyString = ((KeyCode)keyCodes[i]).ToString();
			KeyCode keyCode = (KeyCode)keyCodes[i];

			if (!keys.ContainsKey(keyString))
			{
				keys.Add(keyString, new ButtonKey(keyString, keyCode));
			}
		}
	}

	public static void UpdateTrackedKeys()
	{
		keysPressed = 0;
		for (int i = 0; i < keyCodes.Length; i++)
		{
			if (Input.GetKey((KeyCode)keyCodes[i]))
			{
				keysPressed++;
			}
		}

		if (Input.mouseScrollDelta.y != 0)
		{
			keysPressed++;
		}

	}

	public static List<Key> GetKeysPressed()
	{
		List<Key> foundKeys = new List<Key>();
		var keyNames = new List<string>(keys.Keys);

		for (int i = 0; i < keyNames.Count; i++)
		{
			if (keys[keyNames[i]].IsPressed())
			{
				foundKeys.Add(keys[keyNames[i]]);
			}
		}

		return foundKeys;
	}

	public static List<Key> GetKeysUp()
	{
		List<Key> foundKeys = new List<Key>();
		var keyNames = new List<string>(keys.Keys);

		for (int i = 0; i < keyNames.Count; i++)
		{
			if (keys[keyNames[i]].KeyUp())
			{
				foundKeys.Add(keys[keyNames[i]]);
			}
		}

		return foundKeys;
	}

	public static void AddKeybind(string name, Key key)
	{
		List<Key> keys = new List<Key>();
		keys.Add(key);
		AddKeybind(name, keys);
	}

	public static void AddKeybind(string name)
	{
		AddKeybind(name, new List<Key>());		
	}

	public static void AddKeybind(string name, List<Key> keys)
	{
		if (bindings == null)
		{
			bindings = new Dictionary<string, Keybind>();
		}

		if (!bindings.ContainsKey(name))
		{
			Keybind bind = new Keybind();

			bind.AddKey(keys);
			bindings.Add(name, bind);
		}
		else
		{
			Keybind bind = bindings[name];
			bind.AddKey(keys);
		}
	}

	public static bool GetKey(string keyName)
	{
		return KeyCheck(keyName, KeyEventType.KeyPressed);

	}

	public static bool GetKeyDown(string keyName)
	{
		return KeyCheck(keyName, KeyEventType.KeyDown);
	}

	public static bool GetKeyUp(string keyName)
	{
		return KeyCheck(keyName, KeyEventType.KeyUp);
	}

	static bool KeyCheck(string keyName, KeyEventType type)
	{
		if (bindings.ContainsKey(keyName))
		{
			var keys = bindings[keyName].keyCombos;
			for (int i = 0; i < keys.Count; i++)
			{
				bool positiveMatch = false;
				switch (type)
				{
					case KeyEventType.KeyDown:
						positiveMatch = keys[i].KeyDown();
						break;
					case KeyEventType.KeyPressed:
						positiveMatch = keys[i].IsPressed();
						break;
					case KeyEventType.KeyUp:
						positiveMatch = keys[i].KeyUp();
						break;
				}
				if (positiveMatch)
				{
					return true;
				}
			}
		}
		return false;
	}
}



public struct Keybind
{
	//public string name;
	public List<KeyCombo> keyCombos;

	public bool KeyDown()
	{
		for (int i = 0; i < keyCombos.Count; i++)
		{
			if (keyCombos[i].KeyDown())
			{
				return true;
			}
		}
		return false;
	}

	public bool KeyUp()
	{
		for (int i = 0; i < keyCombos.Count; i++)
		{
			if (keyCombos[i].KeyUp())
			{
				return true;
			}
		}
		return false;
	}

	public bool IsPressed()
	{
		for (int i = 0; i < keyCombos.Count; i++)
		{
			if (keyCombos[i].IsPressed())
			{
				return true;
			}
		}
		return false;
	}

	public void AddKey(List<Key> keys)
	{
		if (keyCombos == null)
		{
			keyCombos = new List<KeyCombo>();
		}

		if (keys.Count == 0)
		{
			return;
		}

		KeyCombo combo = new KeyCombo();
		combo.keys = new List<Key>();
		for (int i = 0; i < keys.Count; i++)
		{
			combo.keys.Add(keys[i]);
		}


		keyCombos.Add(combo);
	}

	bool HasCombo(KeyCombo combo)
	{
		for (int i = 0; i < keyCombos.Count; i++)
		{
			if (keyCombos[i].Matches(combo) )
			{
				return true;
			}
		}
		return false;
	}

	public string GetFirst()
	{
		return keyCombos[0].ToString();
	}
}

public struct KeyCombo
{
	public List<Key> keys;

	public bool IsPressed()
	{
		if (KeyManager.keysPressed != keys.Count)
		{
			return false;
		}

		for (int i = 0; i < keys.Count; i++)
		{
			if (!keys[i].IsPressed())
			{
				return false;
			}
		}
		return true;
	}

	public bool Matches(KeyCombo combo)
	{
		if (combo.keys.Count != keys.Count)
		{
			return false;
		}

		for (int i = 0; i < keys.Count; i++)
		{
			bool found = true;
			for (int j = 0; i < combo.keys.Count; j++)
			{
				if (keys[i] != combo.keys[j])
				{
					found = false;
				}
				else
				{
					found = true;
					break;
				}
			}
			if (!found)
			{
				return false;
			}
		}

		return true;
	}

	bool HasDown()
	{
		for (int i = 0; i < keys.Count; i++)
		{
			if (keys[i].KeyDown())
			{
				return true;
			}
		}
		return false;
	}

	bool HasUp()
	{
		for (int i = 0; i < keys.Count; i++)
		{
			if (keys[i].KeyUp())
			{
				return true;
			}
		}
		return false;
	}

	//the combo was completed this frame
	//all keys are pressed, and at least one is down
	public bool KeyDown()
	{
		return (HasDown() && IsPressed());
	}

	public bool KeyUp()
	{
		return (HasUp() && !IsPressed());
	}

	public override string ToString()
	{
		string str = "";
		for (int i = 0; i < keys.Count; i++)
		{
			str += keys[i].name;
			if (i < keys.Count-1)
			{
				str += " + ";
			}
		}
		return str;
	}
}

public abstract class Key
{
	public string name;
	public abstract bool KeyUp(); 
	public abstract bool KeyDown(); 
	public abstract bool IsPressed(); 
}

public class ButtonKey : Key
{
	KeyCode code;

	public ButtonKey(string nameIn, KeyCode codeIn)
	{
		name = nameIn;
		code = codeIn;
	}

	public override bool IsPressed()
	{
		return Input.GetKey(code);
	}

	public override bool KeyDown()
	{
		return Input.GetKeyDown(code);
	}

	public override bool KeyUp()
	{
		return Input.GetKeyUp(code);
	}
}

public class ScrollKey : Key
{
	public enum Direction
	{
		Up,
		Down
	}

	Direction direction;

	public ScrollKey(Direction directionIn)
	{
		direction = directionIn;
		if (direction == Direction.Up)
		{
			name = "scrollwheel up";
		}
		else
		{
			name = "scrollwheel down";
		}
	}

	public override bool IsPressed()
	{
		return KeyDown();
	}

	public override bool KeyDown()
	{
		var delta = Input.mouseScrollDelta.y;
		return direction == Direction.Up ? delta > 0 : delta < 0;
	}

	public override bool KeyUp()
	{
		return KeyDown();
	}
}