﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Game : MonoBehaviour {

	// Use this for initialization
	void Start () {
		Hotbar.Init();
		KeyManager.Init();
		KeyBindingMenu.Init();
		VideoSettingsMenu.Init();
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
