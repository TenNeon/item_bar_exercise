﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KeyCapture : MonoBehaviour {
	
	// Update is called once per frame
	void Update () {
		var keysUp = KeyManager.GetKeysUp();
		if (keysUp.Count > 0)
		{
			KeyBindingMenu.KeyComboReleased(keysUp);

			Destroy(this.gameObject);
		}
	}




}
