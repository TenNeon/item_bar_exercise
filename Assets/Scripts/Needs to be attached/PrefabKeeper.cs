﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PrefabKeeper : MonoBehaviour
{
	public List<string> prefabNames;
	public List<GameObject> prefabObjects;

	public static Dictionary<string, GameObject> prefabs;

	void Awake()
	{
		prefabs = new Dictionary<string, GameObject>();
		if (prefabNames != null & prefabObjects != null && prefabNames.Count == prefabObjects.Count)
		{
			for (int i = 0; i < prefabNames.Count; i++)
			{
				if (!prefabs.ContainsKey(prefabNames[i]))
				{
					prefabs.Add(prefabNames[i], prefabObjects[i]);
				}
			}
		}
	}

}
