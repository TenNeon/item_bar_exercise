﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HotbarControl : MonoBehaviour {
	public HotbarControl main;

	Dictionary<string, int> hotbarActionMap = new Dictionary<string, int>();
	Dictionary<string, int> tabActionMap = new Dictionary<string, int>();
	// Use this for initialization
	private void Start()
	{
		hotbarActionMap.Add("hotbar 1", 1);
		hotbarActionMap.Add("hotbar 2", 2);
		hotbarActionMap.Add("hotbar 3", 3);
		hotbarActionMap.Add("hotbar 4", 4);
		hotbarActionMap.Add("hotbar 5", 5);
		hotbarActionMap.Add("hotbar 6", 6);
		hotbarActionMap.Add("hotbar 7", 7);
		hotbarActionMap.Add("hotbar 8", 8);
		hotbarActionMap.Add("hotbar 9", 9);
		hotbarActionMap.Add("hotbar 10", 0);

		tabActionMap.Add("tab 1", 1);
		tabActionMap.Add("tab 2", 2);
		tabActionMap.Add("tab 3", 3);
		tabActionMap.Add("tab 4", 4);
		tabActionMap.Add("tab 5", 5);
		tabActionMap.Add("tab 6", 6);
		tabActionMap.Add("tab 7", 7);
	}

	private void Update()
	{
		KeyManager.UpdateTrackedKeys();
		if (Input.anyKeyDown || Input.mouseScrollDelta.y != 0)
		{
			HandleKeyDown();
		}
	}

	void HandleKeyDown()
	{
		if (KeyManager.GetKeyDown("next tab"))
		{
			Hotbar.NextTab();
		}

		if (KeyManager.GetKeyDown("previous tab"))
		{
			Hotbar.PreviousTab();
		}

		var actionKeys = new List<string>(hotbarActionMap.Keys);
		for (int i = 0; i < actionKeys.Count; i++)
		{
			if (KeyManager.GetKeyDown(actionKeys[i]))
			{
				SelectBox(hotbarActionMap[actionKeys[i]]);
			}
		}

		actionKeys = new List<string>(tabActionMap.Keys);
		for (int i = 0; i < actionKeys.Count; i++)
		{
			if (KeyManager.GetKeyDown(actionKeys[i]))
			{
				SelectTab(tabActionMap[actionKeys[i]]);
			}
		}
	}

	void SelectBox(int boxNumber)
	{
		//convert from box number to box index
		int boxIndex = boxNumber - 1;
		boxIndex = boxIndex == -1 ? 9 : boxIndex;
		Hotbar.ToggleBoxSelection(boxIndex);
	}

	void SelectTab(int tabNumber)
	{
		int tabIndex = tabNumber - 1;
		tabIndex = tabIndex == -1 ? Hotbar.tabCount-1 : tabIndex;
		Hotbar.ToggleTabSelection(tabIndex);
	}

}

