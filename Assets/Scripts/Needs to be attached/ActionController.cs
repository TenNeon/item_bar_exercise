﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActionController : MonoBehaviour {
	public static ActionController main;
	public enum ActionState
	{
		Inactive,
		Delete,
		Paint
	}

	public static ActionState state;
	// Use this for initialization
	void Start () {
		if (main == null)
		{
			main = this;
		}
		else
		{
			Destroy(this);
		}
		state = new ActionState();
		SelectionController.SelectionChangeEvent += UpdateSelection;
	}
	
	// Update is called once per frame
	void Update () {
		if (KeyManager.GetKey("action 1"))
		{
			switch (state)
			{
				case ActionState.Inactive:
					break;
				case ActionState.Delete:
					DeleteAction.Step();
					break;
				case ActionState.Paint:
					PaintAction.Step();
					break;
				default:
					break;
			}
		}
	}

	void UpdateSelection(string newSelection)
	{
		switch (newSelection)
		{
			case "delete action":
				state = ActionState.Delete;
				break;
			case "paint action":
				state = ActionState.Paint;
				break;
			default:
				state = ActionState.Inactive;
				break;
		}

		PaintAction.selectedSprite = SelectionController.GetSelectedSprite();
	}
}
