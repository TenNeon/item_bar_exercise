﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MainMenu : MonoBehaviour {
	static GameObject menuObject;

	[SerializeField]
	int menuWidth = 200;
	[SerializeField]
	int menuHeight = 400;
	[SerializeField]
	RectOffset padding = new RectOffset();
	[SerializeField]
	int buttonHeight = 50;

	public string gameName = "Game Name";
	// Use this for initialization
	void Start () {
		BuildMenu();
	}

	private void BuildMenu()
	{
		UIUtility.AttachCanvas(gameObject, RenderMode.ScreenSpaceOverlay);
		menuObject = UIUtility.Box(new Rect(0,0, menuWidth, menuHeight), transform);
		var layout = menuObject.AddComponent<VerticalLayoutGroup>();
		if (layout != null)
		{
			layout.padding = padding;
		}

		UIUtility.Fill = new Color(0.5f,0.5f, 0.95f);
		UIUtility.FontSize = 36;


		var titleObject = UIUtility.Text(menuObject.transform, gameName);
		var continueButtonObject = UIUtility.Button(new Rect(0,0, menuWidth- padding.horizontal, buttonHeight), menuObject.transform, "Continue");
		var newGameButtonObject = UIUtility.Button(new Rect(0, 0, menuWidth - padding.horizontal, buttonHeight), menuObject.transform, "New Game");
		var loadGameButtonObject = UIUtility.Button(new Rect(0, 0, menuWidth - padding.horizontal, buttonHeight), menuObject.transform, "Load Game");
		var optionsButtonObject = UIUtility.Button(new Rect(0, 0, menuWidth - padding.horizontal, buttonHeight), menuObject.transform, "Options");
		var exitButtonObject = UIUtility.Button(new Rect(0, 0, menuWidth - padding.horizontal, buttonHeight), menuObject.transform, "Exit");
	}
}
