﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeleteAction : MonoBehaviour {
	// Use this for initialization
	void Start () {
		
	}

	// Update is called once per frame
	public static void Step()
	{
		if (
			Input.GetMouseButton(0)
			&& SpriteList.main != null
			&& WorldUtility.NotOverUI())
		{
			var hitObj = WorldUtility.CheckForHit();

			if (hitObj != null)
			{
				var renderer = hitObj.GetComponent<SpriteRenderer>();

				if (renderer != null)
				{
					Destroy(hitObj);
				}
			}
		}
	}
}
