﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PaintAction : MonoBehaviour {
	//when the mouse is pressed, create random sprites
	static Transform spriteParent;
	public static Sprite selectedSprite;

	static float nextZ = 990;
	static float zDiff = 0.0001f;

	// Update is called once per frame
	public static void Step () {
		if (spriteParent == null)
		{
			spriteParent = new GameObject("Sprite Parent").transform;
		}
		if ( 
			SpriteList.main != null 
			&& spriteParent != null 
			&& WorldUtility.NotOverUI())
		{
			var hitObj = WorldUtility.CheckForHit();

			if (hitObj != null )
			{
				var renderer = hitObj.GetComponent<SpriteRenderer>();

				if (renderer != null && renderer.sprite != selectedSprite)
				{
					PaintOver(renderer, selectedSprite);
				}
			}
			else
			{
				if (selectedSprite == null)
				{
					selectedSprite = SelectionController.GetSelectedSprite();
				}
				if (selectedSprite != null)
				{
					Paint(selectedSprite);			
				}
				else
				{
					PaintRandom();
				}
			}
		}
	}

	public static GameObject Paint(Sprite spriteToPaint)
	{
		Vector3 pos = Camera.main.ScreenToWorldPoint(Input.mousePosition);

		//this bit helps to enforce new sprites appearing on top of old ones... until the roll-around
		pos.z = nextZ;
		nextZ -= zDiff;

		if (nextZ <= -10f)
		{
			nextZ = 990f;
		}

		var newGO = new GameObject(spriteToPaint.name);
		var sr = newGO.AddComponent<SpriteRenderer>();
		sr.sprite = spriteToPaint;
		newGO.transform.SetParent(spriteParent.transform);
		newGO.transform.position = pos;

		var cc = newGO.AddComponent<CircleCollider2D>();
		cc.radius = cc.radius * 0.8f;

		//var rb = newGO.AddComponent<Rigidbody2D>();
		//rb.gravityScale = 0f;

		return newGO;
	}

	public static void PaintOver(SpriteRenderer targetSpriteRenderer, Sprite newSprite)
	{
		targetSpriteRenderer.sprite = newSprite;
	}

	public static void PaintRandom()
	{
		var randomSprite = SpriteList.main.GetRandomSprite();
		var newGO = Paint(randomSprite);

		newGO.AddComponent<Rigidbody2D>();
	}

}
